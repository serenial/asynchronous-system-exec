set VCPKGROOT="C:\src\vcpkg"
set SCRIPT_DIR=%~dp0.
set BUILD_DIR="%SCRIPT_DIR%\..\.build-win-x86"
echo off
set BUILD_TYPE=release

if "%1" == "" (
     echo *** Building release ***
     echo If a debug version is required supply the batch file with the argument "debug".
)
if "%1" == "debug" (
    set BUILD_TYPE=DEBUG
    echo ** Building debug ***
)


mkdir %BUILD_DIR%
cd %BUILD_DIR%

cmake -DCMAKE_TOOLCHAIN_FILE:STRING="%VCPKGROOT:"=%\scripts\buildsystems\vcpkg.cmake" ^
 -DCMAKE_BUILD_TYPE=%BUILD_TYPE% ^
 ../C++ -G "Visual Studio 16 2019" -T host=x86 -A win32 ^
 && cmake --build . --config %BUILD_TYPE% ^
 && cmake --install . --config %BUILD_TYPE%